#! /usr/bin/env bash



case "$#" in
	"0" )
		${FUSEKI_HOME}/fuseki-server --file=data/demo.ttl /demo
		exit
		;;
	"1" )
		${FUSEKI_HOME}/fuseki-server --file=$1 /demo
		exit
		;;
	* )
		echo "Usage: $0 [path to RDF file]"
		echo "       If no argument is given, default is data/demo.ttl"
		;;
esac

#exit 0

#${FUSEKI_HOME}/fuseki-server --file=data/demo.ttl /demo
