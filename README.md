# rdfCartographer

Python library for extracting a subgraph of an RDF graph.


## Dependencies

- [Python](https://www.python.org/)
- [graphviz library for Python](https://graphviz.readthedocs.io/en/stable/)
- [RDFLib for Python](https://rdflib.readthedocs.io/en/stable/)
- [(optional) Apache fuseki if you do not use a remote endpoint](https://jena.apache.org/download/index.cgi)


## Usage

### 1. connect to a SPARQL endpoint

You can either setup your own endpoint (cf. section 1.1), or use a remote endpoint (section 1.2)

#### 1.1 setup your own endpoint (requires Apache fuseki)

```bash
./startFusekiDemo.bash [path to your RDF file]
```

NB: if you do not specify the path to an RDF file, the default is `data/demo.ttl`.

Your data is now accessible at: [http://localhost:3030/demo/query](http://localhost:3030/demo/query)


#### 1.2 use a remote endpoint

Nothing to do :-)
Just make sure to note the endpoint's URL for the SPARQL protocol (which may or may not be different from the GUI).


### 2. Scenarii

RdfCartographer has been successfuly used in [https://gitlab.com/odameron/uniprotPPI](https://gitlab.com/odameron/uniprotPPI) (teaching project, the goal was to explore the UniProt data model to figure out how to extract protein-protein interactions).
It even allowed to identify a query path simpler than the one we managed to find by manual exploration of the dataset.

Below, we provide a use-case on a simpler dataset.
For the tutorial, we will use the data from [data/demo.ttl](data/demo.ttl) (cf. schema below).

![Graphical representation of the RDF demo dataset from data/demo.ttl](./data/demo.png "demo.ttl dataset")



#### 2.1 create a graph

```python
import RdfCartographer

cartographer = RdfCartographer.RdfCartographer('http://localhost:3030/demo/query')
```


#### 2.2 (optional) declare namespaces

The following prefixes are declared by default:
- `rdf`
- `rdfs`
- `owl`
- `xml`
- `xsd`
- `skos`

If you want to use other prefixes, you can either use the `add_prefix(self, prefixName, prefixValue)` method, or override `reset_graph(self)`.
Note that the `get_prefixes(self)` method returns an iterator over the couples (`prefix_name`, `prefix_value`) where `prefix_name` is a string (e.g. `rdf`), and `prefix_value` is an `rdflib.term.URIRef` (e.g. `rdflib.term.URIRef('http://www.w3.org/1999/02/22-rdf-syntax-ns#')`).

```python
import RdfCartographer

cartographer = RdfCartographer.RdfCartographer('http://localhost:3030/demo/query')
for x in cartographer.get_prefixes():
    print("- {}".format(x))

# - ('xml', rdflib.term.URIRef('http://www.w3.org/XML/1998/namespace'))
# - ('rdf', rdflib.term.URIRef('http://www.w3.org/1999/02/22-rdf-syntax-ns#'))
# - ('rdfs', rdflib.term.URIRef('http://www.w3.org/2000/01/rdf-schema#'))
# - ('xsd', rdflib.term.URIRef('http://www.w3.org/2001/XMLSchema#'))
# - ('owl', rdflib.term.URIRef('http://www.w3.org/2002/07/owl#'))
# - ('skos', rdflib.term.URIRef('http://www.w3.org/2004/02/skos/core#'))

cartographer.add_prefix("ex", "http://www.example.org/")
for x in cartographer.get_prefixes():
    print("- {}".format(x))

# - ('xml', rdflib.term.URIRef('http://www.w3.org/XML/1998/namespace'))
# - ('rdf', rdflib.term.URIRef('http://www.w3.org/1999/02/22-rdf-syntax-ns#'))
# - ('rdfs', rdflib.term.URIRef('http://www.w3.org/2000/01/rdf-schema#'))
# - ('xsd', rdflib.term.URIRef('http://www.w3.org/2001/XMLSchema#'))
# - ('owl', rdflib.term.URIRef('http://www.w3.org/2002/07/owl#'))
# - ('skos', rdflib.term.URIRef('http://www.w3.org/2004/02/skos/core#'))
# - ('ex', rdflib.term.URIRef('http://www.example.org/'))
```


#### 2.3 find the URI of a node you may be interested in

By default, `cartographer.get_items_by_label("some string")` is case sensitive and assumes an exact match:

```python
import RdfCartographer

cartographer = RdfCartographer.RdfCartographer('http://localhost:3030/demo/query')
for currentNode in cartographer.get_items_by_label("individual"):
    print(currentNode)

# Returns: empty
```

does not return anything because no entity has a label `"individual"`.

```python
import RdfCartographer

cartographer = RdfCartographer.RdfCartographer('http://localhost:3030/demo/query')
for currentNode in cartographer.get_items_by_label("individual", caseSensitive=True, exact=False):
    print(currentNode)

# Returns (not necessarily in this order):
# http://www.example.org/indiv7
# http://www.example.org/indiv1
# http://www.example.org/indiv3
# http://www.example.org/indiv2
# http://www.example.org/indiv6
# http://www.example.org/indiv4
# http://www.example.org/indiv5
```

As expected, `cartographer.get_items_by_label("INDivIDual3", caseSensitive=True, exact=True)` returns nothing, whereas `cartographer.get_items_by_label("individual3", caseSensitive=True, exact=True)` returns `http://www.example.org/indiv3`.

Nb: the method `qname(...)` use the known prefixes (see section 2.2) for a nicer display:
```python
import RdfCartographer

cartographer = RdfCartographer.RdfCartographer('http://localhost:3030/demo/query')
for currentNode in cartographer.get_items_by_label("individual", caseSensitive=True, exact=False):
    #print(currentNode)
    print(cartographer.qname(currentNode))

# Returns (not necessarily in this order):
# ex:indiv7
# ex:indiv1
# ex:indiv3
# ex:indiv2
# ex:indiv6
# ex:indiv4
# ex:indiv5
```

#### 2.4 explore around your node of interest (1/3): what are its neighbors?

The `show_neighbors(...)` function displays all the triples involving your node of interest.

```python
import RdfCartographer

cartographer = RdfCartographer.RdfCartographer('http://localhost:3030/demo/query')
cartographer.show_neighbors("http://www.example.org/indiv2")

# Returns (not necessarily in this order):
# http://www.example.org/indiv2	--http://www.w3.org/1999/02/22-rdf-syntax-ns#type-->	http://www.example.org/Person
# http://www.example.org/indiv2	--http://www.w3.org/2004/02/skos/core#altLabel-->	individual2 (from skos:altLabel)
# http://www.example.org/indiv2	--http://www.example.org/knows-->	http://www.example.org/indiv3
# http://www.example.org/indiv2	--http://www.example.org/knows-->	http://www.example.org/indiv4
# http://www.example.org/indiv2	--http://www.example.org/knows-->	http://www.example.org/indiv5
# http://www.example.org/indiv2	--http://www.w3.org/2004/02/skos/core#prefLabel-->	individual2 (from skos:prefLabel)
# http://www.example.org/indiv2	--http://www.w3.org/2000/01/rdf-schema#label-->	individual2
# http://www.example.org/indiv1	--http://www.example.org/knows-->	http://www.example.org/indiv2
```

The `relationDirection` argument (values: `both` (default), `Outbound` or `Inbound`) allows you to focus on inbound or outbound relations.
Note that by default, it display both (last line above).

```python
import RdfCartographer

cartographer = RdfCartographer.RdfCartographer('http://localhost:3030/demo/query')
#cartographer.show_neighbors("http://www.example.org/indiv2", relationDirection="Outbound")
cartographer.show_neighbors("http://www.example.org/indiv2", relationDirection="Inbound")

# Returns:
# http://www.example.org/indiv1	--http://www.example.org/knows-->	http://www.example.org/indiv2
```

#### 2.5 explore around your node of interest (2/3): what are the relationships?

For some node of interest, there may be too many neighbors so we may want to look at the relations involving this node.
The `get_relations(...)` function returns the list of the URI of the properties of the triples involving your node of interest.
The `show_relations(...)` function displays the list of the URI of the properties of the triples involving your node of interest (note that the `useQnames` argument allows you to choose either CURIEs or full URIs).


```python
import RdfCartographer

cartographer = RdfCartographer.RdfCartographer('http://localhost:3030/demo/query')
for currentRelation in cartographer.get_relations("http://www.example.org/indiv2", relationDirection="Outbound"):
    print("{}\t{}".format(type(currentRelation), cartographer.qname(currentRelation)))

# Returns:
# <class 'rdflib.term.URIRef'>	http://www.w3.org/1999/02/22-rdf-syntax-ns#type
# <class 'rdflib.term.URIRef'>	http://www.w3.org/2004/02/skos/core#altLabel
# <class 'rdflib.term.URIRef'>	http://www.example.org/knows
# <class 'rdflib.term.URIRef'>	http://www.w3.org/2004/02/skos/core#prefLabel
# <class 'rdflib.term.URIRef'>	http://www.w3.org/2000/01/rdf-schema#label


for currentRelation in cartographer.get_relations("http://www.example.org/indiv2", relationDirection="Outbound"):
    print("{}\t{}".format(type(currentRelation), cartographer.qname(currentRelation)))

# Returns:
# <class 'rdflib.term.URIRef'>	rdf:type
# <class 'rdflib.term.URIRef'>	skos:altLabel
# <class 'rdflib.term.URIRef'>	ns1:knows
# <class 'rdflib.term.URIRef'>	skos:prefLabel
# <class 'rdflib.term.URIRef'>	rdfs:label

cartographer.show_relations("http://www.example.org/indiv2", relationDirection="Outbound")
# Displays:
# rdf:type
# skos:altLabel
# ns1:knows
# skos:prefLabel
# rdfs:label

cartographer.show_relations("http://www.example.org/indiv2", relationDirection="Outbound", useQnames=False)
# Displays:
# http://www.w3.org/1999/02/22-rdf-syntax-ns
# http://www.w3.org/2004/02/skos/core
# http://www.example.org/knows
# http://www.w3.org/2004/02/skos/core
# http://www.w3.org/2000/01/rdf-schema
```


#### 2.6 explore around your node of interest (3/3): focus on the values of a relation of interest

For some node of interest, there may be too many neighbors.
One you have used the `get_relations(...)` or `show_relations(...)` functions from section 2.5 to identify a particular relation of interest, the `get_neighbors(...)` or `show_neighbors(...)` we have already seen in section 2.4 allow you to focus on this relation.

```python
import RdfCartographer

cartographer = RdfCartographer.RdfCartographer('http://localhost:3030/demo/query')
cartographer.show_neighbors("http://www.example.org/indiv2", relationIdent="http://www.example.org/knows")
# Displays:
# http://www.example.org/indiv2	--http://www.example.org/knows-->	http://www.example.org/indiv3
# http://www.example.org/indiv2	--http://www.example.org/knows-->	http://www.example.org/indiv4
# http://www.example.org/indiv2	--http://www.example.org/knows-->	http://www.example.org/indiv5
# http://www.example.org/indiv1	--http://www.example.org/knows-->	http://www.example.org/indiv2
```

Note that the optional `relationDirection` argument (values: `both` (default), `Outbound` or `Inbound`) allows you to focus on inbound or outbound relations.
Note that by default, it display both (last line above).


#### 2.7 Follow transitive relations (1/2): retrieve all the direct and indirect neighbors

The `get_relation_values(...)` function returns the list of dictionnaries containing the relation of interest's values as well as possibly their type, their label and the label relation (set to `False` by default for improving performances and avoiding the duplication of values with multiple types or labels).
The `relationTransitive` boolean argument specifies whether the relation should be explored recursively.

```python
import RdfCartographer

cartographer = RdfCartographer.RdfCartographer('http://localhost:3030/demo/query')
for currentNeighbor in cartographer.get_relation_values("http://www.example.org/indiv1", "http://www.example.org/knows", relationTransitive=False):
    print(currentNeighbor["neighborValue"])
    for (k, v) in currentNeighbor.items():
        if k != "neighborValue":
            print("  {}\t{}".format(k, v))
# Displays
# http://www.example.org/indiv2
#   neighborLabelValue	None
#   neighborLabelRelation	None
#   neighborType	None

for currentNeighbor in cartographer.get_relation_values("http://www.example.org/indiv1", "http://www.example.org/knows", relationTransitive=True, includeValueLabel = True, includeValueType = True):
    print(currentNeighbor["neighborValue"])
    for (k, v) in currentNeighbor.items():
        if k != "neighborValue":
            print("  {}\t{}".format(k, v))
# Displays
# http://www.example.org/indiv2
#   neighborLabelValue	individual2
#   neighborLabelRelation	http://www.w3.org/2000/01/rdf-schema#label
#   neighborType	http://www.example.org/Person
# http://www.example.org/indiv2
#   neighborLabelValue	individual2 (from skos:prefLabel)
#   neighborLabelRelation	http://www.w3.org/2004/02/skos/core#prefLabel
#   neighborType	http://www.example.org/Person
# http://www.example.org/indiv2
#   neighborLabelValue	individual2 (from skos:altLabel)
#   neighborLabelRelation	http://www.w3.org/2004/02/skos/core#altLabel
#   neighborType	http://www.example.org/Person
# http://www.example.org/indiv3
#   neighborLabelValue	individual3
#   neighborLabelRelation	http://www.w3.org/2000/01/rdf-schema#label
#   neighborType	http://www.example.org/Person
# http://www.example.org/indiv5
#   neighborLabelValue	individual5
#   neighborLabelRelation	http://www.w3.org/2000/01/rdf-schema#label
#   neighborType	http://www.example.org/Person
# http://www.example.org/indiv7
#   neighborLabelValue	individual7
#   neighborLabelRelation	http://www.w3.org/2000/01/rdf-schema#label
#   neighborType	http://www.example.org/Person
# http://www.example.org/indiv6
#   neighborLabelValue	individual6
#   neighborLabelRelation	http://www.w3.org/2000/01/rdf-schema#label
#   neighborType	http://www.example.org/Person
# http://www.example.org/indiv4
#   neighborLabelValue	individual4
#   neighborLabelRelation	http://www.w3.org/2000/01/rdf-schema#label
#   neighborType	http://www.example.org/Person

```

#### 2.8 Follow transitive relations (2/2): retrieve all the steps

The `add_relation_transitive_links(...)` function retrieves all the steps involving the direct and indirect neighbors of a node along a particular relation, and adds them to the default graph. 
It is particularly useful for extracting the graph induced by a relation (including `rdfs:subClassOf` for taxonomies).

```python
import RdfCartographer

cartographer = RdfCartographer.RdfCartographer('http://localhost:3030/demo/query')
cartographer.add_relation_transitive_links("http://www.example.org/indiv1", "http://www.example.org/knows", includeLabel = True, includeType = True)
```

#### 2.9 Retrieve all the instances of a class (and their labels)

The `get_instances(...)` function returns a dictionnary with the instances of a class (key = `neighborValue`) and possibly their labels (key = `neighborLabelValue`, as well as `neighborLabelRelation` if you want to know whether the relation was `rdfs:label`, `skos:prefLabel`, `skos:altLabel`, etc.).

```python
import RdfCartographer

cartographer = RdfCartographer.RdfCartographer('http://localhost:3030/demo/query')
cartographer.add_prefix("ex", "http://www.example.org/")

for currentInstance in cartographer.get_instances("ex:Person"):
    print(currentInstance)

# Displays:
# {'neighborValue': rdflib.term.URIRef('http://www.example.org/indiv2'), 'neighborLabelValue': None, 'neighborLabelRelation': None, 'neighborType': None}
#{'neighborValue': rdflib.term.URIRef('http://www.example.org/indiv7'), 'neighborLabelValue': None, 'neighborLabelRelation': None, 'neighborType': None}
#{'neighborValue': rdflib.term.URIRef('http://www.example.org/indiv3'), 'neighborLabelValue': None, 'neighborLabelRelation': None, 'neighborType': None}
#...
```

#### 2.10 Retrieve all the functions that involve at least one of the instances of a class of interest

The `get_relations_for_instances_of_class(...)` function computes all the direct and indirect instances of a class and retrieves all the relations involving at least one of them.

```python
import RdfCartographer

cartographer = RdfCartographer.RdfCartographer('http://localhost:3030/demo/query')
for currentRelation in cartographer.get_relations_for_instances_of_class("http://www.example.org/Person", relationDirection = "Outbound"):
    print("{}".format(currentRelation))

# Displays:
# http://www.w3.org/1999/02/22-rdf-syntax-ns#type
# http://www.w3.org/2004/02/skos/core#altLabel
# http://www.example.org/knows
# http://www.w3.org/2004/02/skos/core#prefLabel
# http://www.w3.org/2000/01/rdf-schema#label
```



#### 2.11 Craft a subset of the original dataset

The `RdfCartographer` class contains a `rdfGraph` attribute that can be used to construct a subset of the dataset (and possibly also extend it) with a visualization, exploration or extraction purpose.

This `rdfGraph` can be exported using the `saveRDFgraph(...)` function. This function has an optional (default=False) argument `displayImageGenerationInstruction` that displays the command for generating a graphical representation of `rdfGraph`.

Note that `RdfCartographer` also has a `reset_graph(...)` method which sets the `rdfGraph` attribute back to an empty graph. By default, the reset does not affect the declared namespaces (see. section 2.2).

```python
import RdfCartographer

cartographer = RdfCartographer.RdfCartographer('http://localhost:3030/demo/query')
cartographer.add_prefix("ex", "http://www.example.org/")
cartographer.add_relation_transitive_links("ex:indiv1", "ex:knows", includeLabel = False, includeType = False)

cartographer.saveRDFgraph("mySubGraph.ttl", displayImageGenerationInstruction=True)
# Displays:
# rapper --input turtle mySubGraph.ttl --output dot | dot -Tpng -o mySubGraph.png
```

![Graphical representation of the RDF subset (data/mySubGraph.ttl) extracted from the demo dataset from data/demo.ttl](./data/mySubGraph.png "mySubGraph.ttl dataset")

## Todo
