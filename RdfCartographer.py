#! /usr/bin/env python3

import pathlib
import rdflib
from SPARQLWrapper import SPARQLWrapper, JSON



class RdfCartographer:

    def __init__(self, endpointURL):
        """
        Constructor for RdfCartographer with mandatory endpoint URL.

        argument endpointURL -- URL of the dataset's SPARQL endpoint
        """
        self.reset_graph()
        self.endpoint = SPARQLWrapper(endpointURL)
        self.endpoint.setReturnFormat(JSON)
        self.current_node = None
        self.highlighted_nodes = {}
        self.highlighted_relations = set()


    def __repr__(self):
        descr = """RdfCartographer
  endpoint: {}
""".format(self.endpoint.endpoint)

        descr += """
  known prefixes: 
"""
        for currentPrefix in self.get_prefixes():
            descr += "    - {}\t{}\n".format(currentPrefix[0], currentPrefix[1])

        descr += """
  currentNode: {}
""".format(self.current_node)
        return descr


    def reset_graph(self, resetNamespaces=True):
        """
        Remove all the triples from the local graph.

        argument:
        resetNamespaces -- boolean reset the namespaces (default: True)
        """
        if not resetNamespaces:
            formerPrefixes = cartographer.get_prefixes()
        self.rdfGraph = rdflib.Graph()
        self.rdfGraph.bind("rdf", rdflib.namespace.RDF)
        self.rdfGraph.bind("rdfs", rdflib.namespace.RDFS)
        self.rdfGraph.bind("owl", rdflib.namespace.OWL)
        self.rdfGraph.bind("xsd", rdflib.namespace.XSD)
        self.rdfGraph.bind("skos", rdflib.namespace.SKOS)
        if not resetNamespaces:
            for currentPrefix in formerPrefixes:
                cartographer.add_prefix(currentPrefix[0], str(currentPrefix[1]))


    def add_prefix(self, prefixName, prefixValue):
        """
        Add a new prefix declaration.

        Default prefixes:
        - rdf
        - rdfs
        - owl
        - xml
        - xsd
        - skos

        arguments:
        prefixName -- str the name of the prefix (e.g. "rdf")
        prefixValue -- str the local part of the URI (e.g. "http://www.w3.org/1999/02/22-rdf-syntax-ns#")
        """
        self.rdfGraph.bind(prefixName, prefixValue)


    def get_prefixes(self):
        """
        Return the list of known prefixes.
        """
        return self.rdfGraph.namespace_manager.namespaces()


    def get_number_of_triples(self):
        """
        Return the number of triples in the endpoint.
        """
        scriptDir = pathlib.Path(__file__).parent.absolute()
        sparqlQuery = pathlib.Path(scriptDir / 'queries/getNumberOfTriples.rq').read_text()
        self.endpoint.setQuery(sparqlQuery)
        results = self.endpoint.query().convert()
        for result in results["results"]["bindings"]:
            return int(result["nbTriples"]["value"])
        return -1


    def get_classes(self):
        """
        Return the various classes the current element is a direct instance of, or a direct subclass of.
        """
        resultClasses = []
        scriptDir = pathlib.Path(__file__).parent.absolute()
        sparqlQuery = pathlib.Path(scriptDir / 'queries/getClasses.rq').read_text()
        self.endpoint.setQuery(sparqlQuery)
        results = self.endpoint.query().convert()
        for result in results["results"]["bindings"]:
            resultClasses.append(result["class"]["value"])
        return resultClasses


    def get_relations(self, nodeIdent = None, relationDirection = "Outbound"):
        """
        Return the various relations departing from or arriving to the current node.
        
        arguments:
        nodeIdent -- str URI (full or CURIE) of the node
        relationDirection -- "Outbound" or "Inbound" or "Both" (default: "Outbound")
        """
        resultRelations = []
        scriptDir = pathlib.Path(__file__).parent.absolute()
        sparqlQuery = pathlib.Path(scriptDir / str('queries/template_getRelations' + relationDirection + '.rq')).read_text().replace("$nodeIdent$", "?nodeIdent" if nodeIdent == None else self._get_URI(nodeIdent))
        self.endpoint.setQuery(sparqlQuery)
        results = self.endpoint.query().convert()
        for result in results["results"]["bindings"]:
            #resultRelations.append(result["relation"]["value"])
            resultRelations.append(rdflib.URIRef(result["relation"]["value"]))
        return resultRelations


    def show_relations(self, nodeIdent = None, relationDirection = "Outbound", useQnames = True):
        """
        Display the various relations departing from or arriving to the current node.
        
        arguments:
        nodeIdent -- str URI (full or CURIE) of the node
        relationDirection -- "Outbound" or "Inbound" or "Both" (default: "Outbound")
        useQnames -- boolean true if qualified names should be used instead of full URIS whenever possible (default: True)
        """
        for currentRelation in self.get_relations("http://www.example.org/indiv2", relationDirection="Outbound"):
            if useQnames:
                print("{}".format(self.qname(currentRelation)))
            else:
                print("{}".format(self.rdfGraph.absolutize(currentRelation)))


    def get_relations_for_instances_of_class(self, classIdent, relationDirection = "Outbound"):
        """
        Return the list of the URIs of the relations that involve at leat one (in)direct instance of the class.
        
        arguments:
        classIdent -- str URI (full or CURIE) of the class. The query retrieves its direct and indirect instances
        relationDirection -- "Outbound" or "Inbound" or "Both" (default: "Outbound")
        """
        resultRelations = []
        scriptDir = pathlib.Path(__file__).parent.absolute()
        sparqlQuery = pathlib.Path(scriptDir / str('queries/template_getRelationsForInstances' + relationDirection + '.rq')).read_text().replace("$classIdent$", self._get_URI(classIdent))
        self.endpoint.setQuery(sparqlQuery)
        results = self.endpoint.query().convert()
        for result in results["results"]["bindings"]:
            resultRelations.append(result["relation"]["value"])
        return resultRelations


    def get_relations_for_instances_of_same_type(self, instanceIdent, relationDirection = "Outbound"):
        """
        Return the list of the URIs of the relations that involve at leat one other (in)direct instance of the same classes.
        
        arguments:
        instanceIdent -- str URI (full or CURIE) of the instance used as model. 
        relationDirection -- "Outbound" or "Inbound" or "Both" (default: "Outbound")
        """
        resultRelations = []
        scriptDir = pathlib.Path(__file__).parent.absolute()
        sparqlQuery = pathlib.Path(scriptDir / str('queries/template_getRelationsForInstancesOfSameType' + relationDirection + '.rq')).read_text().replace("$instanceIdent$", self._get_URI(instanceIdent))
        self.endpoint.setQuery(sparqlQuery)
        results = self.endpoint.query().convert()
        for result in results["results"]["bindings"]:
            resultRelations.append(result["relation"]["value"])
        return resultRelations


    def _get_URI(self, nodeIdent):
        """
        Return a Turtle or SPARQL compatible representation of an URI, by delimitating it with '<' and '>' if it is not a CURIE.
        
        arguments:
        nodeIdent -- str URI (full or CURIE) of the node
        """
        # FIXME: should be a class method
        nodeIdent = str(nodeIdent)
        if nodeIdent.startswith("http"):
            return "<" + nodeIdent + ">"
        return nodeIdent


    def _get_full_URI(self, qname):
        """
        Return a ful version of an URI by replacing the prefix in CURIEs by their associated value.
        """
        #qname = self.rdfGraph.absolutize(qname)
        #qname = self.rdfGraph.namespace_manager.absolutize(qname)
        qname = self.rdfGraph.namespace_manager.expand_curie(qname)
        if qname.startswith("http"):
            return qname
        for currentPrefix, currentNS in self.rdfGraph.namespace_manager.namespaces():
            if qname.startswith(currentPrefix + ":"):
                return qname.replace(currentPrefix + ":", currentNS)
        return qname


    def qname(self, uri):
        """
        Return the qualified name of an URI if possible
        """
        return self.rdfGraph.qname(uri)


    def _node_exists(self, nodeIdent):
        """
        Return true iff the resource exists in the original dataset
        """
        scriptDir = pathlib.Path(__file__).parent.absolute()
        sparqlQuery = pathlib.Path(scriptDir / 'queries/template_askNodeExists.rq').read_text().replace("$nodeIdent$", self._get_URI(nodeIdent))
        self.endpoint.setQuery(sparqlQuery)
        results = self.endpoint.query().convert()
        return results['boolean']


    def set_current_node(self, nodeIdent):
        """
        Set the current node of the local dataset
        """
        # TODO: check whether nodeIdent exists?
        self.current_node = nodeIdent


    def get_items_by_label(self, label, additionalLabelRelations=[], caseSensitive=True, exact=True):
        """
        Return the list of entities associated to a label (rdfs:label|skos:prefLabel|skos:altLabel).
        
        arguments:
        label -- string the label value
        additionalLabelRelations -- list of additional label relations to consider (default: empty)
        caseSensitive -- boolean True if the case of entity's label matches the case of the label argument (default: True)
        exact -- boolean True if the match should be exact; False if the entity's label contains the string of the label argument (default: True)
        """
        scriptDir = pathlib.Path(__file__).parent.absolute()
        sparqlQuery = pathlib.Path(scriptDir / 'queries/template_getNodesByLabel.rq').read_text()
        additionalLabelRelationsClause = ""
        for currentRelation in additionalLabelRelations:
            additionalLabelRelationsClause += "|<{}>".format(self._get_full_URI(currentRelation))
        sparqlQuery = sparqlQuery.replace("$additionalLabelRelations$", additionalLabelRelationsClause)
        if exact:
            if caseSensitive:
                sparqlQuery = sparqlQuery.replace("$labelClause$", "str(?nodeLabel) = \"" + label + "\"")
            else:
                #sparqlQuery = sparqlQuery.replace("$labelClause$", "LCASE(str(?nodeLabel)) = LCASE(\"" + label + "\")")
                sparqlQuery = sparqlQuery.replace("$labelClause$", "LCASE(str(?nodeLabel)) = \"" + label.lower() + "\"")
        else:
            if caseSensitive:
                sparqlQuery = sparqlQuery.replace("$labelClause$", "CONTAINS(?nodeLabel, \"" + label + "\")")
            else:
                #sparqlQuery = sparqlQuery.replace("$labelClause$", "CONTAINS(LCASE(?nodeLabel), LCASE(\"" + label + "\"))")
                sparqlQuery = sparqlQuery.replace("$labelClause$", "CONTAINS(LCASE(?nodeLabel), \"" + label.lower() + "\")")
        self.endpoint.setQuery(sparqlQuery)
        results = self.endpoint.query().convert()
        resultNodes = []
        for result in results["results"]["bindings"]:
            resultNodes.append(result["nodeIdent"]["value"])
        return resultNodes


    def show_neighbors(self, nodeIdent, relationIdent = None, relationDirection = "Both"):
        """
        Display a node's neighbor, either for all relations or for a specified one.

        arguments:
        nodeIdent -- str URI (full or CURIE) of the node
        relationIdent -- str URI (full or CURIE) of the relation, or None for all considering all relations (default: None)
        relationDirection -- "Outbound" or "Inbound" or "Both" (default: "Outbound")
        """
        # TODO: add parameter relationDirection ["Inbound" | "Outbound" | "Both"]
        scriptDir = pathlib.Path(__file__).parent.absolute()
        sparqlQuery = pathlib.Path(scriptDir / str('queries/template_getNodeNeighbors' + relationDirection + '.rq')).read_text().replace("$nodeIdent$", self._get_URI(nodeIdent))
        if relationDirection == "Inbound" or relationDirection == "Both":
            sparqlQuery = sparqlQuery.replace("$inboundRelationValues$", "" if relationIdent == None else "VALUES ?inboundRelation { " + self._get_URI(relationIdent) + "}")
        if relationDirection == "Outbound" or relationDirection == "Both":
            sparqlQuery = sparqlQuery.replace("$outboundRelationValues$", "" if relationIdent == None else "VALUES ?outboundRelation { " + self._get_URI(relationIdent) + "}")
        for currentPrefix, currentNS in self.rdfGraph.namespace_manager.namespaces():
            if "PREFIX {}: ".format(currentPrefix) not in sparqlQuery:
                sparqlQuery = "PREFIX {}: <{}>\n".format(currentPrefix, currentNS) + sparqlQuery
        self.endpoint.setQuery(sparqlQuery)
        results = self.endpoint.query().convert()
        for result in results["results"]["bindings"]:
            #print(result)
            if "outboundRelation" in result.keys():
                # TODO: check whether neighbor is an URI or a literal, and display its qname if URI
                print("{}\t--{}-->\t{}".format(nodeIdent, self.qname(result["outboundRelation"]["value"]), result["neighbor"]["value"]))
            elif "inboundRelation" in result.keys():
                # TODO: temporary removal of self.qname(...) for the subject because of bad URIs in uniprot
                print("{}\t--{}-->\t{}".format(result["neighbor"]["value"], self.qname(result["inboundRelation"]["value"]), nodeIdent))


    def add_neighbors(self, nodeIdent, relationIdent = None, relationDirection = "Both"):
        """
        Add the neighbors of a node to the local graph.

        arguments:
        nodeIdent -- str URI (full or CURIE) of the node
        relationIdent -- str URI (full or CURIE) of the relation, or None for all considering all relations (default: None)
        relationDirection -- "Outbound" or "Inbound" or "Both" (default: "Outbound")
        """
        ## TODO: add parameter relationDirection ["Inbound" | "Outbound" | "Both"]
        #scriptDir = pathlib.Path(__file__).parent.absolute()
        #sparqlQuery = pathlib.Path(scriptDir / str('queries/template_getNodeNeighbors' + relationDirection + '.rq')).read_text().replace("$nodeIdent$", self._get_URI(nodeIdent))
        #if relationDirection == "Inbound" or relationDirection == "Both":
        #    sparqlQuery = sparqlQuery.replace("$inboundRelationValues$", "" if relationIdent == None else "VALUES ?inboundRelation { " + self._get_URI(relationIdent) + "}")
        #if relationDirection == "Outbound" or relationDirection == "Both":
        #    sparqlQuery = sparqlQuery.replace("$outboundRelationValues$", "" if relationIdent == None else "VALUES ?outboundRelation { " + self._get_URI(relationIdent) + "}")
        #for currentPrefix, currentNS in self.rdfGraph.namespace_manager.namespaces():
        #    if "PREFIX {}: ".format(currentPrefix) not in sparqlQuery:
        #        sparqlQuery = "PREFIX {}: <{}>\n".format(currentPrefix, currentNS) + sparqlQuery
        #self.endpoint.setQuery(sparqlQuery)
        #results = self.endpoint.query().convert()
        #for result in results["results"]["bindings"]:
        #    if "outboundRelation" in result.keys():
        #        print("{}\t--{}-->\t{}".format(nodeIdent, result["outboundRelation"]["value"], result["neighbor"]["value"]))
        #        if result["neighbor"]["type"] == "uri":
        #            self.rdfGraph.add((rdflib.URIRef(nodeIdent), rdflib.URIRef(result["outboundRelation"]["value"]), rdflib.URIRef(result["neighbor"]["value"])))
        #        else:
        #            self.rdfGraph.add((rdflib.URIRef(nodeIdent), rdflib.URIRef(result["outboundRelation"]["value"]), rdflib.Literal(result["neighbor"]["value"])))
        #    elif "inboundRelation" in result.keys():
        #        print("{}\t--{}-->\t{}".format(result["neighbor"]["value"], result["inboundRelation"]["value"], nodeIdent))
        #        if result["neighbor"]["type"] == "uri":
        #           self.rdfGraph.add((rdflib.URIRef(result["neighbor"]["value"]), rdflib.URIRef(result["inboundRelation"]["value"]), rdflib.URIRef(nodeIdent)))
        #        #else:
        #        #   self.rdfGraph.add((rdflib.URIRef(nodeIdent), rdflib.URIRef(result["outboundRelation"]["value"]), rdflib.Literal(result["neighbor"]["value"])))
        neighbors = self.get_neighbors(nodeIdent, relationIdent, relationDirection)
        for currentNeighbor in neighbors:
            if "outboundRelation" in currentNeighbor.keys():
                self.rdfGraph.add( (rdflib.URIRef(nodeIdent), currentNeighbor["outboundRelation"], currentNeighbor["neighbor"]) )
            elif "inboundRelation" in currentNeighbor.keys():
                self.rdfGraph.add( (currentNeighbor["neighbor"], currentNeighbor["inboundRelation"], rdflib.URIRef(nodeIdent)) )


    def get_neighbors(self, nodeIdent, relationIdent = None, relationDirection = "Both"):
        """
        Return the list of neighbors of a node to the local graph.

        arguments:
        nodeIdent -- str URI (full or CURIE) of the node
        relationIdent -- str URI (full or CURIE) of the relation, or None for all considering all relations (default: None)
        relationDirection -- "Outbound" or "Inbound" or "Both" (default: "Outbound")
        """
        # TODO: add parameter relationDirection ["Inbound" | "Outbound" | "Both"]
        neighbors = []
        scriptDir = pathlib.Path(__file__).parent.absolute()
        sparqlQuery = pathlib.Path(scriptDir / str('queries/template_getNodeNeighbors' + relationDirection + '.rq')).read_text().replace("$nodeIdent$", self._get_URI(nodeIdent))
        if relationDirection == "Inbound" or relationDirection == "Both":
            sparqlQuery = sparqlQuery.replace("$inboundRelationValues$", "" if relationIdent == None else "VALUES ?inboundRelation { " + self._get_URI(relationIdent) + "}")
        if relationDirection == "Outbound" or relationDirection == "Both":
            sparqlQuery = sparqlQuery.replace("$outboundRelationValues$", "" if relationIdent == None else "VALUES ?outboundRelation { " + self._get_URI(relationIdent) + "}")
        for currentPrefix, currentNS in self.rdfGraph.namespace_manager.namespaces():
            if "PREFIX {}: ".format(currentPrefix) not in sparqlQuery:
                sparqlQuery = "PREFIX {}: <{}>\n".format(currentPrefix, currentNS) + sparqlQuery
        self.endpoint.setQuery(sparqlQuery)
        results = self.endpoint.query().convert()
        for result in results["results"]["bindings"]:
            if "outboundRelation" in result.keys():
                print("{}\t--{}-->\t{}".format(nodeIdent, self.qname(result["outboundRelation"]["value"]), result["neighbor"]["value"]))
                if result["neighbor"]["type"] == "uri":
                    #self.rdfGraph.add((rdflib.URIRef(nodeIdent), rdflib.URIRef(result["outboundRelation"]["value"]), rdflib.URIRef(result["neighbor"]["value"])))
                    neighbors.append({"outboundRelation":rdflib.URIRef(result["outboundRelation"]["value"]), "neighbor":rdflib.URIRef(result["neighbor"]["value"])})
                else:
                    #self.rdfGraph.add((rdflib.URIRef(nodeIdent), rdflib.URIRef(result["outboundRelation"]["value"]), rdflib.Literal(result["neighbor"]["value"])))
                    neighbors.append({"outboundRelation":rdflib.URIRef(result["outboundRelation"]["value"]), "neighbor":rdflib.Literal(result["neighbor"]["value"])})
            elif "inboundRelation" in result.keys():
                print("{}\t--{}-->\t{}".format(result["neighbor"]["value"], result["inboundRelation"]["value"], nodeIdent))
                if result["neighbor"]["type"] == "uri":
                    #self.rdfGraph.add((rdflib.URIRef(result["neighbor"]["value"]), rdflib.URIRef(result["inboundRelation"]["value"]), rdflib.URIRef(nodeIdent)))
                    neighbors.append({"inboundRelation":rdflib.URIRef(result["inboundRelation"]["value"]), "neighbor":rdflib.URIRef(result["neighbor"]["value"])})
                #else:
                #   self.rdfGraph.add((rdflib.URIRef(nodeIdent), rdflib.URIRef(result["outboundRelation"]["value"]), rdflib.Literal(result["neighbor"]["value"])))
        return neighbors


    def get_instances(self, classIdent, indirectInstances=False, includeValueLabel = False):
        """
        Return the list of instances of a class.

        arguments:
        classIdent -- str URI (full or CURIE) of the class of interest
        indirectInstances -- boolean whether to retrieve the indirect instances of the class, i.e. the instances of its descendants (default False)
        includeValueLabel -- boolean whether to also retrieve the instances' labels (default: False)
        """
        return self.get_relation_values(classIdent, "rdf:type", relationDirection = "Inbound", relationTransitive = indirectInstances, includeValueLabel = includeValueLabel, includeValueType = False)
        #return [currentInstance['neighborValue'] for currentInstance in self.get_relation_values(classIdent, "rdf:type", relationDirection = "Inbound", relationTransitive = indirectInstances, includeValueLabel = includeValueLabel, includeValueType = False)]
    #
    # import RdfCartographer
    # 
    # cartographer = RdfCartographer.RdfCartographer('http://localhost:3030/demo/query')
    # cartographer.add_prefix("ex", "http://www.example.org/")
    # 
    # for currentInstance in cartographer.get_instances("ex:Person"):
    #     print(currentInstance)
    #     #print(cartographer.qname(currentInstance))
    #     ##print(currentInstance['neighborValue'])
    #     ##print(cartographer.qname(currentInstance['neighborValue']))


    def show_instances(self, classIdent, indirectInstances=False, includeValueLabel = False):
        """
        Display the list of instances of a class.

        arguments:
        classIdent -- str URI (full or CURIE) of the class of interest
        indirectInstances -- boolean whether to retrieve the indirect instances of the class, i.e. the instances of its descendants (default False)
        includeValueLabel -- boolean whether to also retrieve the instances' labels (default: False)
        """
        for currentInstance in self.get_instances(classIdent, indirectInstances, includeValueLabel):
            print(self.qname(currentInstance['neighborValue']))
            if includeValueLabel:
                print("    " + currentInstance['neighborLabelValue'] + " (through " + self.qname(currentInstance['neighborLabelRelation']) + ")")


    def add_relation_values(self, nodeIdent, relationIdent, relationDirection = "Outbound", relationTransitive = False, includeValueLabel = False, includeValueType = False):
        """
        Add all the values of a relation to the local graph.

        arguments:
        nodeIdent -- str URI (full or CURIE) of the node of reference
        relationIdent -- str URI (full or CURIE) of the relation
        relationDirection -- "Outbound" or "Inbound" or "Both" (default: "Outbound")
        relationTransitive -- boolean whether to consider the relation to be transitive (default: False)
        includeValueLabel -- boolean whether to also retrieve (and add to the local graph) the instances' labels (default: False)
        includeValueType -- boolean whether to also retrieve (and add to the local graph) the instances' types (default: False)
        """
        # TODO: add parameter relationDirection ["Inbound" | "Outbound"]
        scriptDir = pathlib.Path(__file__).parent.absolute()
        sparqlQuery = pathlib.Path(scriptDir / str('queries/template_getRelationValues' + relationDirection + '.rq')).read_text().replace("$nodeIdent$", self._get_URI(nodeIdent)).replace("$relationIdent$", self._get_URI(relationIdent)).replace("$relationTransitive$", "+" if relationTransitive else "").replace("$neighborLabelVariable$", "?neighborLabelRelation ?neighborLabel" if includeValueLabel else "").replace("$neighborLabelClause$", "OPTIONAL {\n    ?neighbor rdfs:label|skos:prefLabel|skos:altLabel ?neighborLabel .\n    ?neighbor ?neighborLabelRelation ?neighborLabel .\n  }" if includeValueLabel else "").replace("$neighborTypeVariable$", "?neighborType" if includeValueType else "").replace("$neighborTypeClause$", "OPTIONAL { ?neighbor rdf:type ?neighborType }" if includeValueType else "")
        for currentPrefix, currentNS in self.rdfGraph.namespace_manager.namespaces():
            if "PREFIX {}: ".format(currentPrefix) not in sparqlQuery:
                sparqlQuery = "PREFIX {}: <{}>\n".format(currentPrefix, currentNS) + sparqlQuery
        self.endpoint.setQuery(sparqlQuery)
        results = self.endpoint.query().convert()
        for result in results["results"]["bindings"]:
            if relationDirection == "Outbound":
                print("{}\t--{}-->\t{}".format(nodeIdent, relationIdent, result["neighbor"]["value"]))
                if result["neighbor"]["type"] == "uri":
                    self.rdfGraph.add((rdflib.URIRef(nodeIdent), rdflib.URIRef(relationIdent), rdflib.URIRef(result["neighbor"]["value"])))
                elif result["neighbor"]["type"] == "bnode":
                    self.rdfGraph.add((rdflib.URIRef(nodeIdent), rdflib.URIRef(relationIdent), rdflib.BNode(result["neighbor"]["value"])))
                else:
                    self.rdfGraph.add((rdflib.URIRef(nodeIdent), rdflib.URIRef(relationIdent), rdflib.Literal(result["neighbor"]["value"])))
            elif relationDirection == "Inbound":
                print("{}\t--{}-->\t{}".format(result["neighbor"]["value"], relationIdent, nodeIdent))
                if result["neighbor"]["type"] == "uri":
                    self.rdfGraph.add((rdflib.URIRef(result["neighbor"]["value"]), rdflib.URIRef(relationIdent), rdflib.URIRef(nodeIdent)))
                elif result["neighbor"]["type"] == "bnode":
                    self.rdfGraph.add((rdflib.URIRef(nodeIdent), rdflib.URIRef(relationIdent), rdflib.BNode(result["neighbor"]["value"])))
                #else:
                #   self.rdfGraph.add((rdflib.URIRef(nodeIdent), rdflib.URIRef(relationIdent), rdflib.Literal(result["neighbor"]["value"])))
            if "neighborType" in result.keys():
                if result["neighborType"]["type"] == "uri":
                    self.rdfGraph.add((rdflib.URIRef(result["neighbor"]["value"]), rdflib.namespace.RDF.type, rdflib.URIRef(result["neighborType"]["value"])))
            if "neighborLabel" in result.keys() and "neighborLabelRelation" in result.keys():
                self.rdfGraph.add((rdflib.URIRef(result["neighbor"]["value"]), rdflib.URIRef(result["neighborLabelRelation"]["value"]), rdflib.Literal(result["neighborLabel"]["value"])))


    def get_relation_values(self, nodeIdent, relationIdent, relationDirection = "Outbound", relationTransitive = False, includeValueLabel = False, includeValueType = False):
        """
        Return the list of the values of a relation to the local graph.

        arguments:
        nodeIdent -- str URI (full or CURIE) of the node of reference
        relationIdent -- str URI (full or CURIE) of the relation
        relationDirection -- "Outbound" or "Inbound" or "Both" (default: "Outbound")
        relationTransitive -- boolean whether to consider the relation to be transitive (default: False)
        includeValueLabel -- boolean whether to also retrieve (and add to the local graph) the instances' labels (default: False)
        includeValueType -- boolean whether to also retrieve (and add to the local graph) the instances' types (default: False)
        """
        # TODO: add parameter relationDirection ["Inbound" | "Outbound"]
        relationValues = []
        scriptDir = pathlib.Path(__file__).parent.absolute()
        sparqlQuery = pathlib.Path(scriptDir / str('queries/template_getRelationValues' + relationDirection + '.rq')).read_text().replace("$nodeIdent$", self._get_URI(nodeIdent)).replace("$relationIdent$", self._get_URI(relationIdent)).replace("$relationTransitive$", "+" if relationTransitive else "").replace("$neighborLabelVariable$", "?neighborLabelRelation ?neighborLabel" if includeValueLabel else "").replace("$neighborLabelClause$", "OPTIONAL {\n    ?neighbor rdfs:label|skos:prefLabel|skos:altLabel ?neighborLabel .\n    ?neighbor ?neighborLabelRelation ?neighborLabel .\n  }" if includeValueLabel else "").replace("$neighborTypeVariable$", "?neighborType" if includeValueType else "").replace("$neighborTypeClause$", "OPTIONAL { ?neighbor rdf:type ?neighborType }" if includeValueType else "")
        for currentPrefix, currentNS in self.rdfGraph.namespace_manager.namespaces():
            if "PREFIX {}: ".format(currentPrefix) not in sparqlQuery:
                sparqlQuery = "PREFIX {}: <{}>\n".format(currentPrefix, currentNS) + sparqlQuery
        self.endpoint.setQuery(sparqlQuery)
        results = self.endpoint.query().convert()
        for result in results["results"]["bindings"]:
            currentNeighborDescription = {"neighborValue":None, "neighborLabelValue":None, "neighborLabelRelation":None, "neighborType":None}

            if result["neighbor"]["type"] == "uri":
                currentNeighborDescription["neighborValue"] = rdflib.URIRef(result["neighbor"]["value"])
            elif result["neighbor"]["type"] == "bnode":
                currentNeighborDescription["neighborValue"] = rdflib.BNode(result["neighbor"]["value"])
            else:
                currentNeighborDescription["neighborValue"] = rdflib.Literal(result["neighbor"]["value"])

            if "neighborType" in result.keys() and result["neighborType"]["type"] == "uri":
                currentNeighborDescription["neighborType"] = rdflib.URIRef(result["neighborType"]["value"])

            if "neighborLabel" in result.keys() and "neighborLabelRelation" in result.keys():
                currentNeighborDescription["neighborLabelRelation"] = rdflib.URIRef(result["neighborLabelRelation"]["value"])
                currentNeighborDescription["neighborLabelValue"] = rdflib.Literal(result["neighborLabel"]["value"])

            relationValues.append(currentNeighborDescription)
        return relationValues


    def get_ancestors(self, nodeIdent, includeValueLabel = False, includeValueType = False):
        """
        Return the list of direct and indirect superclasses.

        arguments:
        nodeIdent -- str URI (full or CURIE) of the node of reference
        includeValueLabel -- boolean whether to also retrieve (and add to the local graph) the instances' labels (default: False)
        includeValueType -- boolean whether to also retrieve (and add to the local graph) the instances' types (default: False)
        """
        return self.get_relation_values(nodeIdent, relationIdent, relationDirection = "Outbound", relationTransitive = True, includeValueLabel = False, includeValueType = False)


    def get_descendants(self, nodeIdent, includeValueLabel = False, includeValueType = False):
        """
        Return the list of direct and indirect subclasses.

        arguments:
        nodeIdent -- str URI (full or CURIE) of the node of reference
        includeValueLabel -- boolean whether to also retrieve (and add to the local graph) the instances' labels (default: False)
        includeValueType -- boolean whether to also retrieve (and add to the local graph) the instances' types (default: False)
        """
        return self.get_relation_values(nodeIdent, relationIdent, relationDirection = "Inbound", relationTransitive = True, includeValueLabel = False, includeValueType = False)


    def add_relation_transitive_links(self, nodeIdent, relationIdent, relationDirection = "Outbound", includeLabel = False, includeType = False, filterType=None):
        """
        Add a transitive chain of triples following a relation (e.g. for retrieving a hierarchical structure) to the local graph.

        arguments:
        nodeIdent -- str URI (full or CURIE) of the node of reference
        relationIdent -- str URI (full or CURIE) of the relation
        relationDirection -- "Outbound" or "Inbound" or "Both" (default: "Outbound")
        includeValueLabel -- boolean whether to also retrieve (and add to the local graph) the instances' labels (default: False)
        includeValueType -- boolean whether to also retrieve (and add to the local graph) the instances' types (default: False)
        filterType -- optional, type of the relation values to retrieve, e.g. for discarding OWL constraints (default: None)
        """
        # TODO: add parameter relationDirection ["Inbound" | "Outbound"]
        scriptDir = pathlib.Path(__file__).parent.absolute()
        #sparqlQuery = pathlib.Path(scriptDir / str('queries/template_getRelationTransitiveLinks' + relationDirection + '.rq')).read_text().replace("$nodeIdent$", self._get_URI(nodeIdent)).replace("$relationIdent$", self._get_URI(relationIdent)).replace("$nodeFromLabelVariable$", "?nodeFromLabel ?nodeFromLabelRelation" if includeLabel else "").replace("$nodeToLabelVariable$", "?nodeToLabel ?nodeToLabelRelation" if includeLabel else "").replace("$nodeFromTypeVariable$", "?nodeFromType" if includeType else "").replace("$nodeToTypeVariable$", "?nodeToType" if includeType else "").replace("$nodeFromLabelClause$", "OPTIONAL { ?nodeFrom rdfs:label|skos:prefLabel|skos:altLabel ?nodeFromLabel .\n    ?nodeFrom ?nodeFromLabelRelation ?nodeFromLabel .\n  }" if includeLabel else "").replace("$nodeToLabelClause$", "OPTIONAL { ?nodeTo rdfs:label|skos:prefLabel|skos:altLabel ?nodeToLabel .\n    ?nodeTo ?nodeToLabelRelation ?nodeToLabel .\n  }" if includeLabel else "").replace("$nodeFromTypeClause$", "OPTIONAL { ?nodeFrom rdf:type ?nodeFromType }" if includeType else "").replace("$nodeToTypeClause$", "OPTIONAL { ?nodeTo rdf:type ?nodeToType }" if includeType else "")
        sparqlQuery = pathlib.Path(scriptDir / str('queries/template_getRelationTransitiveLinks' + relationDirection + '.rq')).read_text().replace("$nodeIdent$", self._get_URI(nodeIdent)).replace("$relationIdent$", self._get_URI(relationIdent)).replace("$nodeFromLabelVariable$", "?nodeFromLabel ?nodeFromLabelRelation" if includeLabel else "").replace("$nodeToLabelVariable$", "?nodeToLabel ?nodeToLabelRelation" if includeLabel else "").replace("$nodeFromTypeVariable$", "?nodeFromType" if includeType else "").replace("$nodeToTypeVariable$", "?nodeToType" if includeType else "").replace("$nodeFromLabelClause$", "OPTIONAL { ?nodeFrom rdfs:label|skos:prefLabel|skos:altLabel ?nodeFromLabel .\n    ?nodeFrom ?nodeFromLabelRelation ?nodeFromLabel .\n  }" if includeLabel else "").replace("$nodeToLabelClause$", "OPTIONAL { ?nodeTo rdfs:label|skos:prefLabel|skos:altLabel ?nodeToLabel .\n    ?nodeTo ?nodeToLabelRelation ?nodeToLabel .\n  }" if includeLabel else "").replace("$nodeFromTypeClause$", "OPTIONAL { ?nodeFrom rdf:type ?nodeFromType }" if includeType else "").replace("$nodeToTypeClause$", "OPTIONAL { ?nodeTo rdf:type ?nodeToType }" if includeType else "").replace("$nodeFromTypeFilter$", "?nodeFrom rdf:type " + filterType + " .\n" if filterType != None else "").replace("$nodeToTypeFilter$", "?nodeTo rdf:type " + filterType + " .\n" if filterType != None else "")
        for currentPrefix, currentNS in self.rdfGraph.namespace_manager.namespaces():
            if "PREFIX {}: ".format(currentPrefix) not in sparqlQuery:
                sparqlQuery = "PREFIX {}: <{}>\n".format(currentPrefix, currentNS) + sparqlQuery
        self.endpoint.setQuery(sparqlQuery)
        results = self.endpoint.query().convert()
        for result in results["results"]["bindings"]:
            #self.rdfGraph.add((rdflib.URIRef(result["nodeFrom"]["value"]), rdflib.URIRef(relationIdent), rdflib.URIRef(result["nodeTo"]["value"])))
            self.rdfGraph.add((rdflib.URIRef(result["nodeFrom"]["value"]), rdflib.URIRef(self._get_full_URI(relationIdent)), rdflib.URIRef(result["nodeTo"]["value"])))
            if "nodeFromLabel" in result.keys() and "nodeFromLabelRelation" in result.keys():
                self.rdfGraph.add((rdflib.URIRef(result["nodeFrom"]["value"]), rdflib.URIRef(result["nodeFromLabelRelation"]["value"]), rdflib.Literal(result["nodeFromLabel"]["value"])))
            if "nodeToLabel" in result.keys() and "nodeToLabelRelation" in result.keys():
                self.rdfGraph.add((rdflib.URIRef(result["nodeTo"]["value"]), rdflib.URIRef(result["nodeToLabelRelation"]["value"]), rdflib.Literal(result["nodeToLabel"]["value"])))
            if "nodeFromType" in result.keys():
                self.rdfGraph.add((rdflib.URIRef(result["nodeFrom"]["value"]), rdflib.namespace.RDF.type, rdflib.URIRef(result["nodeFromType"]["value"])))
            if "nodeToType" in result.keys():
                self.rdfGraph.add((rdflib.URIRef(result["nodeTo"]["value"]), rdflib.namespace.RDF.type, rdflib.URIRef(result["nodeToType"]["value"])))


    def highlight_node(self, nodeIdent, color = "red"):
        """
        Register the node to be highlighted in the visual representations of the local graph.

        arguments:
        nodeIdent -- str URI (full or CURIE) of the node of reference
        color -- str the color to be used for highlighting (default: "red")
        """
        if rdflib.URIRef(self._get_full_URI(nodeIdent)) in self.rdfGraph.all_nodes():
            self.highlighted_nodes[self._get_full_URI(nodeIdent)] = color


    def highlight_relation(self, relationIdent, subjectIdent = None, objectIdent = None, color = "red"):
        """
        Register the relation to be highlighted in the visual representations of the local graph.

        arguments:
        relationIdent -- str URI (full or CURIE) of the relation
        subjectIdent -- str URI (full or CURIE) of the subject of the relation (default: None)
        objectIdent -- str URI (full or CURIE) of the subject of the relation (default: None)
        color -- str the color to be used for highlighting (default: "red")
        """
        if (subjectIdent == None) and (objectIdent == None):
            for (subj, prop, obj) in self.rdfGraph:
                if prop == self._get_full_URI(relationIdent):
                    self.highlighted_relations.add((subj, prop, obj), color)
        elif (subjectIdent == None):
            for subj in self.rdfGraph.subjects(self._get_full_URI(relationIdent), self._get_full_URI(objectIdent), unique=True):
                self.highlighted_relations.add(((subj, rdflib.URIRef(self._get_full_URI(relationIdent)), rdflib.URIRef(self._get_full_URI(objectIdent))), color))
        elif (objectIdent == None):
            for obj in self.rdfGraph.objects(self._get_full_URI(subjectIdent), self._get_full_URI(relationIdent), unique=True):
                self.highlighted_relations.add(((rdflib.URIRef(self._get_full_URI(subjectIdent)), rdflib.URIRef(self._get_full_URI(relationIdent)), obj), color))
        else:
            if (rdflib.URIRef(self._get_full_URI(subjectIdent)), rdflib.URIRef(self._get_full_URI(relationIdent)), rdflib.URIRef(self._get_full_URI(objectIdent))) in self.rdfGraph:
                self.highlighted_relations.add(((rdflib.URIRef(self._get_full_URI(subjectIdent)), rdflib.URIRef(self._get_full_URI(relationIdent)), rdflib.URIRef(self._get_full_URI(objectIdent))), color))


    def save_RDF_graph(self, file_name, rdfFormat = "turtle", displayImageGenerationInstruction=False):
        """
        Export the local RDF graph to a file.

        arguments:
        file_name -- str path to the file where the local RDF graph should be saved
        rdfFormat -- str one of the rdflib file formats: "xml", "n3", "turtle", "nt", "pretty-xml", "trix", "trig", "nquads", "json-ld" and "hext" (default: "turtle")
        displayImageGenerationInstruction -- boolean set to True do display the rapper command to execute in order to generate a graphical representation of the graph (default: False)
        """
        self.rdfGraph.serialize(destination=file_name, format=rdfFormat)
        if displayImageGenerationInstruction:
            imageFileFormat="png"
            print("rapper --input {} {} --output dot | dot -T{} -o {}".format(rdfFormat, file_name, imageFileFormat, pathlib.Path(file_name).with_suffix('.'+imageFileFormat)))





if __name__ == '__main__':
    print('RdfCartographer')
    print('TODO: check arguments')
    cartographer = RdfCartographer('http://localhost:3030/demo/query')
    cartographer.get_number_of_triples()
    print()
    print(cartographer)
    print()
    print("Classes:")
    for currentClass in cartographer.get_classes():
        print("  - {}".format(currentClass))
    print()
    print(cartographer._node_exists("http://www.example.org/indiv1"))
    print(cartographer._node_exists("http://www.example.org/unicorn"))
    #####
    print()
    cartographer.show_neighbors("http://www.example.org/indiv2")
    print()
    cartographer.show_neighbors("http://www.example.org/indiv2", relationDirection="Outbound")
    print()
    cartographer.show_neighbors("http://www.example.org/indiv2", relationDirection="Inbound")
    #####
    print()
    cartographer.add_neighbors("http://www.example.org/indiv2")
    cartographer.save_RDF_graph("jnk.ttl")
    #####
    print()
    cartographer.show_neighbors("http://www.example.org/indiv2", relationIdent="http://www.example.org/knows")
    print()
    cartographer.show_neighbors("http://www.example.org/indiv2", relationIdent="http://www.w3.org/1999/02/22-rdf-syntax-ns#type")
    print()
    #####
    print()
    cartographer.add_relation_values("http://www.example.org/indiv1", "http://www.example.org/knows", relationTransitive=True)
    cartographer.save_RDF_graph("jnk2.ttl")
    #####
    print()
    cartographer.add_relation_values("http://www.example.org/indiv1", "http://www.example.org/knows", relationTransitive=True, includeValueType=True)
    cartographer.save_RDF_graph("jnk3.ttl")
    #####
    print()
    cartographer.add_relation_values("http://www.example.org/indiv1", "http://www.example.org/knows", relationTransitive=True, includeValueLabel = True, includeValueType=True)
    cartographer.save_RDF_graph("jnk4.ttl")
    #####
    print()
    for currentRelation in cartographer.get_relations(nodeIdent = "http://www.example.org/Person"):
        print("  --> {}".format(currentRelation))
    print()
    for currentRelation in cartographer.get_relations(nodeIdent = "http://www.example.org/Person", relationDirection = "Inbound"):
        print("  --> {}".format(currentRelation))
    print()
    for currentRelation in cartographer.get_relations(nodeIdent = "http://www.example.org/Person", relationDirection = "Both"):
        print("  --> {}".format(currentRelation))
    print()
    #####
    print()
    cartographer = RdfCartographer('http://localhost:3030/demo/query')
    cartographer.add_relation_transitive_links("http://www.example.org/indiv1", "http://www.example.org/knows", includeLabel = False, includeType = False)
    cartographer.save_RDF_graph("jnk5.ttl")
    print()
    cartographer = RdfCartographer('http://localhost:3030/demo/query')
    cartographer.add_relation_transitive_links("http://www.example.org/indiv1", "http://www.example.org/knows", includeLabel = True, includeType = False)
    cartographer.save_RDF_graph("jnk6.ttl")
    print()
    cartographer = RdfCartographer('http://localhost:3030/demo/query')
    cartographer.add_relation_transitive_links("http://www.example.org/indiv1", "http://www.example.org/knows", includeLabel = False, includeType = True)
    cartographer.save_RDF_graph("jnk7.ttl")
    print()
    cartographer = RdfCartographer('http://localhost:3030/demo/query')
    cartographer.add_relation_transitive_links("http://www.example.org/indiv1", "http://www.example.org/knows", includeLabel = True, includeType = True)
    cartographer.save_RDF_graph("jnk8.ttl")
    print()
    cartographer = RdfCartographer('http://localhost:3030/demo/query')
    cartographer.add_relation_transitive_links("http://www.example.org/indiv1", "http://www.example.org/knows", includeLabel = True, includeType = True)
    cartographer.add_prefix("example", "http://www.example.org/")
    cartographer.save_RDF_graph("jnk9.ttl")
    print()
    print("Case sensitive, exact, right input")
    for currentNode in cartographer.get_items_by_label("individual5"):
        print(currentNode)
    print("Case sensitive, exact, wrong input")
    for currentNode in cartographer.get_items_by_label("INDivIDual5"):
        print(currentNode)
    print("Case insensitive, exact, right input")
    for currentNode in cartographer.get_items_by_label("individual5", caseSensitive=False):
        print(currentNode)
    print("Case insensitive, exact, wrong input")
    for currentNode in cartographer.get_items_by_label("INDivIDual5", caseSensitive=False):
        print(currentNode)
    print("Case sensitive, contains, right input")
    for currentNode in cartographer.get_items_by_label("indiv", caseSensitive=True, exact=False):
        print(currentNode)
    print("Case sensitive, contains, wrong input")
    for currentNode in cartographer.get_items_by_label("INDiv", caseSensitive=False):
        print(currentNode)


